////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <SFML/Graphics.hpp>
#include <iostream>

////////////////////////////////////////////////////////////
/// Entry point of application
///
/// \return Application exit code
///
////////////////////////////////////////////////////////////
int main()
{
  // Create main window
  sf::RenderWindow App(sf::VideoMode(800, 600), "Playing with Sprites", sf::Style::Fullscreen);
  sf::Clock Clock;

  sf::Image ImageSheet;
  sf::Sprite ShipSprite;

  int shipWidth = 80;
  int shipSpeed = 150;
  int frame = 0;
  int framecount = 6;
  int frameIndexes[6] = { 0, 1, 2, 3, 2, 1 };
  float timePerFrame = .15f;
  float timeElapsed = 0.0f;
  float scale = 1.0f;
  float scaleStep = 1.5f;

  if (!ImageSheet.LoadFromFile("./resources/spriteSheet.png"))
  {
    std::cout << "Error Loading Image" << std::endl;
  }
  ShipSprite.SetImage(ImageSheet);

  ShipSprite.SetSubRect(sf::IntRect(0,77,80,117));

  // Start game loop
  while (App.IsOpened())
  {
    // Process events
    sf::Event Event;
    while (App.GetEvent(Event))
    {
      // Close window : exit
      if (Event.Type == sf::Event::Closed)
      App.Close();
    }

    float ElapsedTime = App.GetFrameTime();


    timeElapsed += ElapsedTime;

    if ( timeElapsed > timePerFrame )
    {
      frame++;
      frame = frame % framecount;
      timeElapsed = 0.0f;
    }
    ShipSprite.SetSubRect(sf::IntRect(
      frameIndexes[frame] * shipWidth,
      77,
      (frameIndexes[frame] + 1) * shipWidth,
      116
    ));

    if (App.GetInput().IsKeyDown(sf::Key::Left))  {
      ShipSprite.FlipX(false);
      ShipSprite.Move(-shipSpeed * ElapsedTime, 0);
    }
    if (App.GetInput().IsKeyDown(sf::Key::Right)) {
      ShipSprite.FlipX(true);
      ShipSprite.Move( shipSpeed * ElapsedTime, 0);
    }
    if (App.GetInput().IsKeyDown(sf::Key::Up)) {
      ShipSprite.Move(0, -shipSpeed * ElapsedTime);
    }
    if (App.GetInput().IsKeyDown(sf::Key::Down)) {
      ShipSprite.Move(0,  shipSpeed * ElapsedTime);
    }
    if (App.GetInput().IsKeyDown(sf::Key::Equal)) {
      scale += scaleStep * ElapsedTime;
      ShipSprite.SetScaleY(scale);
      ShipSprite.SetScaleX(scale);
    }
    if (App.GetInput().IsKeyDown(sf::Key::Dash)) {
      scale -= scaleStep * ElapsedTime;
      ShipSprite.SetScaleY(scale);
      ShipSprite.SetScaleX(scale);
    }

    if (App.GetInput().IsKeyDown(sf::Key::Escape)) {
      break;
    }
              

    // Clear screen
    App.Clear(sf::Color(200, 0, 0));

    // Draw
    App.Draw(ShipSprite);

    // Finally, display the rendered frame on screen
    App.Display();
  }

  return EXIT_SUCCESS;
}
